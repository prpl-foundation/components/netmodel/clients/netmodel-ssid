# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v0.1.3 - 2024-04-28(07:40:26 +0000)

### Fixes

- - sync MACAddress and BSSID with WiFi.SSID

## Release v0.1.2 - 2022-12-21(11:08:54 +0000)

### Other

- Opensource component

## Release v0.1.1 - 2022-10-06(08:39:54 +0000)

### Fixes

- [Netmodel] remove lowerlayers parameter from ssid and radio

## Release v0.1.0 - 2022-08-25(07:51:17 +0000)

### New

- [netmodel][Wifi SSID] Implement a SSID WiFi client in netmodel

